package com.tarento.courses.service;

import com.tarento.courses.exception.SomethingWentWrong;

public class StringJumbleGenerator {

	public String createString(String stringP, String stringQ, String stringR) throws SomethingWentWrong {
		int len = stringP.length();
		if (len != stringQ.length() || len != stringR.length()) {
			throw new SomethingWentWrong("All string must be of the same length");
		}
		String result = generateJumbledString(stringP, stringQ, stringR, 0, len - 1);
		return result;
	}

	private String generateJumbledString(String stringP, String stringQ, String stringR, int index, int limit) {
		StringBuffer sb = new StringBuffer();
		sb.append(stringP.charAt(index)).append(stringQ.charAt(index)).append(stringR.charAt(index));
		if (index != limit) {
			String subString = generateJumbledString(stringP, stringQ, stringR, index + 1, limit);
			sb.append(subString);
		}
		return sb.toString();
	}

}
