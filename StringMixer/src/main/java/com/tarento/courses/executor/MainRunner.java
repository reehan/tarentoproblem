/**
 * 
 */
package com.tarento.courses.executor;

import com.tarento.courses.exception.SomethingWentWrong;
import com.tarento.courses.service.StringJumbleGenerator;
import com.tarento.courses.utils.Constants;
import com.tarento.courses.utils.PrintUtil;

public class MainRunner implements Constants {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StringJumbleGenerator sg = new StringJumbleGenerator();
		String resultString = null;
		System.out.println("Before Length:" + STRING_P.length());
		try {
			resultString = sg.createString(STRING_P, STRING_Q, STRING_R);
		} catch (SomethingWentWrong e) {
			System.out.println("Error:" + e.getMessage());
			e.printStackTrace();
			return;
		}
		PrintUtil.printResult(resultString);
		System.out.println("After length:" + resultString.length());
	}
}
