package com.tarento.courses.utils;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

	public static final List<String> splitOnCommaAndCreateCourses(String inputString) {
		if (inputString == null || inputString.isEmpty()) {
			return null;
		}
		String[] splits = inputString.split(",");
		List<String> splitList = new ArrayList<String>();

		for (String string : splits) {
			if (!(null == string || string.isEmpty()))
				splitList.add(string.trim());
		}

		return splitList;
	}
}
