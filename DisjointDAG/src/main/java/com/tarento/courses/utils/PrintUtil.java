package com.tarento.courses.utils;

import java.util.List;
import java.util.Stack;

import com.tarento.courses.data.Course;

public class PrintUtil {

	public static void printAllCourseDetails(Course root) {

		if (root == null) {
			return;
		}
		Stack<Course> stack = new Stack<Course>();
		stack.push(root);
		System.out.println("===============================");
		while (!stack.isEmpty()) {
			root = stack.pop();
			System.out.print("Course:" + root.getName());
			printAndAddAllSubCoursesToStack(stack, root);
			System.out.println();
		}
		System.out.println("===============================");
	}
	
	private static Stack<Course> printAndAddAllSubCoursesToStack(Stack<Course> stack, Course root){
		List<Course> subcourses = root.getSubCourses();
		// Only if there are some sub courses let us do the processing
		if (subcourses != null && !subcourses.isEmpty()) {
			System.out.print(" -> ");
			for (Course subCourse : subcourses) {
				System.out.print(subCourse.getName() + ", ");
				// This courses needs to be processed to find if there are sub-courses
				stack.push(subCourse);
			}
		}
		return stack;
		
	}

}
