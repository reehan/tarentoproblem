package com.tarento.courses.utils;

public interface Constants {

	final static String COURSES_FILE = "courses.txt";

	final static String ROOT_ELEMENT_CODE = "root";
}
