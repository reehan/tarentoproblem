package com.tarento.courses.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is used to read file and get the data in required format.
 * 
 * @author ideoholic
 *
 */
public class FileReader {

	private Properties prop = new Properties();

	public FileReader(String fileName) throws IOException {
		prop = new Properties();
		InputStream inputStream;

		inputStream = getClass().getClassLoader().getResourceAsStream(fileName);

		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + fileName + "' not found in the classpath");
		}
	}

	public String getProperty(String propertyName) {
		String value = prop.getProperty(propertyName);
		return value;
	}

}
