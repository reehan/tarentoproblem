/**
 * 
 */
package com.tarento.courses.executor;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.tarento.courses.data.Course;
import com.tarento.courses.exception.SomethingWentWrong;
import com.tarento.courses.service.CourseTreeCreator;
import com.tarento.courses.utils.Constants;
import com.tarento.courses.utils.FileReader;
import com.tarento.courses.utils.PrintUtil;

public class MainRunner implements Constants {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileReader properties;
		try {
			properties = new FileReader(COURSES_FILE);
			Course rootCourse = CourseTreeCreator.createCourseTree(properties);
			PrintUtil.printAllCourseDetails(rootCourse);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SomethingWentWrong e) {
			System.out.println("Custom excpetion:" + e.getMessage());
			e.printStackTrace();
		}
	}

}
