/**
 * 
 */
package com.tarento.courses.data;

import java.util.LinkedList;
import java.util.List;

public class Course {

	private String name;
	private List<Course> subCourses;

	/**
	 * 
	 */
	public Course(String name) {
		this.name = name;
		this.subCourses = new LinkedList<Course>();
	}

	public String getName() {
		return name;
	}

	public void addSubCourse(Course subCourse) {
		subCourses.add(subCourse);
	}

	public List<Course> getSubCourses() {
		return subCourses;
	}

}
