package com.tarento.courses.service;

import java.util.LinkedList;
import java.util.List;

import com.tarento.courses.data.Course;
import com.tarento.courses.exception.SomethingWentWrong;
import com.tarento.courses.utils.Constants;
import com.tarento.courses.utils.FileReader;
import com.tarento.courses.utils.StringUtils;

public class CourseTreeCreator implements Constants {

	private List<Course> coursesList = new LinkedList<Course>();

	private CourseTreeCreator() {
	}

	public static Course createCourseTree(FileReader properties) throws SomethingWentWrong {
		Course root = null;
		CourseTreeCreator ctr = new CourseTreeCreator();
		root = ctr.createCourseTree(properties.getProperty(ROOT_ELEMENT_CODE), properties);
		return root;
	}

	private Course createCourseTree(String courseName, FileReader properties) throws SomethingWentWrong {
		if (courseName == null || courseName.isEmpty()) {
			throw new SomethingWentWrong("No root course name given");
		}
		Course root = createCourse(courseName);
		coursesList.add(root);
		while (!coursesList.isEmpty()) {
			Course c = coursesList.remove(0);

			String subCourseStr = properties.getProperty(c.getName());
			// In case there are no courses that can be taken based on this course then it is leaf node 
			if (courseName == null || courseName.isEmpty()) {
				// No processing needed, return
				continue;
			}
			// Based on each of the listed courses, create 
			List<String> subCourses = StringUtils.splitOnCommaAndCreateCourses(subCourseStr);
			if (subCourses != null) {
				for (String subCourseName : subCourses) {
					Course subC = createCourse(subCourseName);
					coursesList.add(subC);
					c.addSubCourse(subC);
				}
			}
		}
		return root;
	}

	private Course createCourse(String courseName) {
		Course c = new Course(courseName);
		return c;
	}

}
