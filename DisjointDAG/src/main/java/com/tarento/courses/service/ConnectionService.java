/**
 * 
 */
package com.tarento.courses.service;

import com.tarento.courses.data.Course;

/**
 * This is the class that checks if the given courses are connected
 *
 */
public interface ConnectionService {

    /**
     * This is the method that checks for the connection to exist between two
     * courses.<br/>
     * 
     * @param mainCourse
     * @param subCourse
     * @return
     */
    public boolean checkCourseIsSubcourse(Course mainCourse, Course subCourse);

}
