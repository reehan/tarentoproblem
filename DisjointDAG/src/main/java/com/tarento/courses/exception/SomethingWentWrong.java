package com.tarento.courses.exception;

public class SomethingWentWrong extends Exception {
	private static final long serialVersionUID = -2859591533021812795L;

	public SomethingWentWrong(String errMsg) {
		super(errMsg);
	}

}
